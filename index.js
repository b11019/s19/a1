// alert("HI");

/*
    1. Declare 3 variables without initialization called username,password and role.
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/


/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called avg.
        -calculate the average of the 4 number inputs and store it in the variable avg.
        -add an if statement to check if the value of avg is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is F"
        -add an else if statement to check if the value of avg is greater than or equal to 75 and if avg is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is D"
        -add an else if statement to check if the value of avg is greater than or equal to 80 and if avg is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is C"
        -add an else if statement to check if the value of avg is greater than or equal to 85 and if avg is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is B"
        -add an else if statement to check if the value of avg is greater than or equal to 90 and if avg is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is A"
        -add an else if statement to check if the value of avg is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/


				// SOLUTION 
/*PROBLEM I*/
		




let userName = "Zephie98";
let passWord = "123456"
let role = "Web Dev"

console.log(userName, passWord, role);



/*PROBLEM II*/

let message = " "
let userAdmin = {
	user: 'userA',
	pass: '123456'
};
let userTeacher = {
	user: 'userB',
	pass: '654321'
};
let userRookie = {
	user: 'userC',
	pass: '073098'

};

function userIdentity(userID, userPass,){
	
	if(user === userAdmin.user && pass === userAdmin.pass){
		
		return message = 'Welcome back Admin'
	} else if(user === userTeacher.user && pass === userTeacher.pass){
		
		return message =  'Welcome back Teacher'
	} else if (user === userRookie.user && pass === userRookie.pass){
		
		return message =  'Welcome back Student'
	} else {
		return message =  'Role out of range'
	};
};
let user = prompt("Enter your ID");
let pass = prompt("Enter your Password")
let role3 = prompt("Enter your role")


userIdentity(user, pass,)
alert(message);


/*PROBLEM III*/

let message2 = "No Message"
// console.log(message2)

function avg (grd1 , grd2, grd3 , grd4){
	let average = (grd1 + grd2 + grd3 + grd4) / 4

	if(average <= 74){
		return 'Hello, student, your average is ' + average + ' ' + 'The letter equivalent is F';
	} else if (average >= 75 && average <= 79){
		return 'Hello, student, your average is ' + average + ' ' + 'The letter equivalent is D';
	} else if (average >= 80 && average <= 84){
		return 'Hello, student, your average is ' + average + ' ' + 'The letter equivalent is C';
	} else if (average >= 85 && average <= 89){
		return 'Hello, student, your average is ' + average + ' ' + 'The letter equivalent is B'
	} else if(average >= 90 && average <= 95){
		return 'Hello, student, your average is ' + average + ' ' + 'The letter equivalent is A'
	} else (average >= 96)
		return 'Hello, student, your average is ' + average + ' ' + 'The letter equivalent is A+'
};	

	message2 = avg(70,72,73,74);
	 console.log("Averagechecker" + ' ' + 70,72,73,74 );
	 console.log(message2);

	message2 = avg(77,76,75,78);
	console.log("Averagechecker" + ' ' + 77,76,75,78 );
	 console.log(message2);

	 message2 = avg(80,79,82,84);
	console.log("Averagechecker" + ' ' + 80,79,82,84 );
	 console.log(message2);

	 message2 = avg(86,83,90,85);
	console.log("Averagechecker" + ' ' + 86,83,90,85 );
	 console.log(message2);

	 message2 = avg(89,93,90,91);
	console.log("Averagechecker" + ' ' + 89,93,90,91 );
	 console.log(message2);

	 message2 = avg(96,93,97,98);
	console.log("Averagechecker" + ' ' + 96,93,97,98 );
	 console.log(message2);

	
	


